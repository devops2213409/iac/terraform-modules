# Terraform Modules

## This repository contains various Terraform modules for AWS and Azure

#### Structure
```
terraform-modules
├── README.md                  # description
├── .gitignore                 # Service file Git
├── .gitlab-ci.yml             # A Git file that contains a series of steps and execution rules of pipeline.
├── .pre-commit-config.yaml    # configuration file to automatically call commands on every commit
└── build/                     # Folder for building the current repository
    ├── Dockerfile             # Docker file to build the current repository
├── AWS/                       # Global folder for models applicable to AWS
    ├── docs/                  # documents for models applicable to AWS
    ├── example/               # example for models applicable to AWS
    └── modules/               # folder for models applicable to AWS
        ├── ecr/               # module 'ecr'
        ├── eks/               # module 'eks'
        ├── kms/               # module 'kms'
        ├── networking/        # subfolder for models 'networking'
            ├── sg/            # module 'sg'
            ├── vpc/           # module 'vpc'
        └── rds/demo           # module 'rds'
└── AZURE/                     # Global folder for models applicable to Azure
    ├── docs/                  # documents for models applicable to Azure
    ├── example/               # folder for models applicable to Azure
    └── modules/               # folder for models applicable to Azure
```
## How to interact with the repository
- [x] Install [pre-commit](https://pre-commit.com/)
    * Pre-commit - is an open source command line tool, part of a toolkit for shifting certain aspects of security left by adding automatic checkpoints after every commit.
- [x] Run ```pre-commit install``` in root directory of the repository to set up the git hook scripts
- [x] Install dependency for terraform [pre-commit-terraform](https://github.com/antonbabenko/pre-commit-terraform)
- [x] Run(Firstly) ```pre-commit run --all-files```
